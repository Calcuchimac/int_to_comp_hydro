import h5py
import numpy as np
import matplotlib.pyplot as plt
import sys


f_name=str(sys.argv[1])
print "file=", f_name
f=h5py.File(f_name,'r')
a=np.array(f.get('a(x,t)'))
x=np.array(f.get('x'))
print "a= ",a
print "x= ",x
print "Starting the plot" 

fig,axs=plt.subplots()

axs.plot(x,a[25,:],'b*')

axs.set_ylabel('a')
axs.set_xlabel('x')
plt.show()


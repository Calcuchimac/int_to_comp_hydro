program tres
	implicit none
	real :: e_old(100),e_new(100), x(100),dx
	integer :: i
	logical :: log_write
	log_write=.true.
	dx=( 0.90 - 0.1)/100.0
	do i=1,100
		x(i)=0.1 +i*dx
	enddo
	e_old=sin(x) -(x-x**3.0/6.0) 
	x=x/2
	
	e_new=sin(x) -(x-x**3.0/6.0) 

	if (log_write) then
        	write(*,*) "x= ",x
		write(*,*) "e_old= ",e_old
		write(*,*) "e_new= ",e_new
                write(*,*) "e_old/e_new",e_old/e_new 
	endif
end program tres

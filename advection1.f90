program advection
	use hdf5
	implicit none
	real,allocatable    :: a(:,:),x(:)
	real ::c,delta_t,u,delta_x,x_i,x_f,t
	integer :: i,n,n_t,n2_t,j,hdferr,rank=2,rankx=1
	integer(HID_T) :: file, group,dspace_id,dset_id

	character(LEN=16), parameter :: filename="advention_5.hdf5"
	integer(hsize_t), dimension(2) :: dims,dimsx
	

	n=100
	x_i=0.0
	x_f=1.0
	u=1.0
	c=0.7
	n_t=3
	t=n_t*1.0/u
	delta_x=(x_f-x_i)/n
	delta_t=c*delta_x/u
	n2_t=int(t/delta_t)
	dimsx=n+2
	write(*,*) "the number of time integrations is: ",n2_t, t, delta_t 
	allocate(a(0:n+1,1:n2_t+1),x(0:n+1))

	do i=0,n+1
		x(i)=x_i+i*delta_x
		if(x(i)<1.0/3.0) then
			a(i,:)=0.0
		else if ( x(i)>=1.0/3.0 .and. x(i)< 2.0/3.0) then
			a(i,:)=1.0
		else
			a(i,:)=0.0
		endif
	enddo
	!write(*,*) "x= ",x
	!write(*,*) "a_x= ",a_x
	do j=1,n2_t
		do i=1,n
			a(i,j+1)= a(i,j)-u*delta_t*(a(i,j)-a(i-1,j))/delta_x
	
		enddo
	enddo
	dims=(/n+2,n2_t+1/)
	!write(*,*) "a =",a
	call h5open_f(hdferr)
	call h5fcreate_f(filename,h5f_acc_trunc_f,file,hdferr)

	call h5screate_simple_f(rank,dims,dspace_id,hdferr)
	call h5dcreate_f(file,'a(x,t)',h5t_native_real,dspace_id,dset_id,hdferr)
	call h5dwrite_f(dset_id,h5t_native_real,a,dims,hdferr)
	call h5dclose_f(dset_id,hdferr)
	call h5sclose_f(dspace_id,hdferr)

	call h5screate_simple_f(rankx,dimsx,dspace_id,hdferr)
	call h5dcreate_f(file,'x',h5t_native_real,dspace_id,dset_id,hdferr)
	call h5dwrite_f(dset_id,h5t_native_real,x,dims,hdferr)
	call h5dclose_f(dset_id,hdferr)
	call h5sclose_f(dspace_id,hdferr)


	call h5close_f(hdferr)
	deallocate(a,x)
end program advection

